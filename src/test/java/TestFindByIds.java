import com.qianfeng.bean.EntryBook;
import com.qianfeng.dao.EntryBookDao;
import com.qianfeng.dao.impl.EntryBookDaoImpl;
import java.util.List;

/**
 * 二涵upup
 * 2019/11/6 17:21
 */
public class TestFindByIds
{
    public static void main(String[] args) {
        EntryBookDao entryBookDao = new EntryBookDaoImpl();
        int[] arr = {1,3,5};
        List<EntryBook> findbyids = entryBookDao.findbyids(arr);
        for (EntryBook entryBook : findbyids)
        {
            System.out.println(entryBook);
        }
    }
}
