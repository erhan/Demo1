import com.qianfeng.bean.EntryBook;
import com.qianfeng.dao.impl.EntryBookDaoImpl;

import java.util.List;

/**
 * 二涵upup
 * 2019/11/5 19:13
 */
public class TestFindById
{
    public static void main(String[] args)
    {
        EntryBookDaoImpl entryBookDao = new EntryBookDaoImpl();
        //findById
        List<EntryBook> list = entryBookDao.findById(5, 2);
        for (Object entryBook : list)
        {
            System.out.println(entryBook);
        }

    }

}
