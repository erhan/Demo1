import com.qianfeng.bean.EntryBook;
import com.qianfeng.dao.EntryBookDao;
import com.qianfeng.dao.impl.EntryBookDaoImpl;

import java.util.List;

/**
 * 二涵upup
 * 2019/11/6 21:26
 */
public class TestFindBetween
{
    public static void main(String[] args)
    {
        EntryBookDao entryBookDao = new EntryBookDaoImpl();
        List<EntryBook> list = entryBookDao.findBetween("2018-08-22", "2019-10-14");
        for (EntryBook entryBook : list)
        {
            System.out.println(entryBook);
        }
    }
}
