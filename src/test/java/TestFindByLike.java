import com.qianfeng.bean.EntryBook;
import com.qianfeng.dao.EntryBookDao;
import com.qianfeng.dao.impl.EntryBookDaoImpl;

import java.util.List;

/**
 * 二涵upup
 * 2019/11/6 19:47
 */
public class TestFindByLike
{
    public static void main(String[] args)
    {
        EntryBookDao entryBookDao = new EntryBookDaoImpl();
        List<EntryBook> list = entryBookDao.findByLike("Python", "aaaa");
        for (EntryBook entryBook : list)
        {
            System.out.println(entryBook);
        }
    }

}
