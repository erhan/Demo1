import com.qianfeng.bean.EntryBook;
import com.qianfeng.dao.impl.EntryBookDaoImpl;

/**
 * 二涵upup
 * 2019/11/5 19:13
 */
public class TestUpdate
{
    public static void main(String[] args)
    {
        EntryBookDaoImpl entryBookDao = new EntryBookDaoImpl();
        //update
        int update = entryBookDao.update(new EntryBook(5, 2, "HTML545", "H5", "erhan", null));
        System.out.println(update);
    }
}
