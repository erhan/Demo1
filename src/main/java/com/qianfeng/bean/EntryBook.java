package com.qianfeng.bean;

import java.util.Date;

/**
 * 二涵upup
 * 2019/11/5 15:45
 */
public class EntryBook
{
    private Integer id;
    private Integer categoryid;
    private String title;
    private String summary;
    private String uploaduser;
    private Date creatdate;

    @Override
    public String toString()
    {
        return "id=" + id + ", categoryid=" + categoryid + ", title='" + title + '\'' + ", summary='" + summary + '\'' + ", uploaduser='" + uploaduser + '\'' + ", creatdate=" + creatdate + '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(Integer categoryid) {
        this.categoryid = categoryid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getUploaduser() {
        return uploaduser;
    }

    public void setUploaduser(String uploaduser) {
        this.uploaduser = uploaduser;
    }

    public Date getCreatdate() {
        return creatdate;
    }

    public void setCreatdate(Date creatdate) {
        this.creatdate = creatdate;
    }

    public EntryBook() {
    }

    public EntryBook(Integer id, Integer categoryid, String title, String summary, String uploaduser, Date creatdate) {
        this.id = id;
        this.categoryid = categoryid;
        this.title = title;
        this.summary = summary;
        this.uploaduser = uploaduser;
        this.creatdate = creatdate;
    }
}
