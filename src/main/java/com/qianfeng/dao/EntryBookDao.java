package com.qianfeng.dao;

import com.qianfeng.bean.EntryBook;

import java.lang.ref.SoftReference;
import java.util.List;
import java.util.Map;

/**
 * 二涵upup
 * 2019/11/5 15:47
 */
public interface EntryBookDao
{
    public List<EntryBook> findall();

    public int insert(EntryBook entryBook);

    public int update(EntryBook entryBook);

    public int delete(int id);

    public List<EntryBook> findById(int id, int  categoryid);

    public Map findJuHe();

    public List<EntryBook> findbyids(int[] ids);

    public List<EntryBook> findByLike(String title,String summary);

    public List<EntryBook> findBetween(String begin, String end);
}
