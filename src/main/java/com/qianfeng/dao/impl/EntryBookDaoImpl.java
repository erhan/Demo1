package com.qianfeng.dao.impl;

import com.qianfeng.bean.EntryBook;
import com.qianfeng.dao.EntryBookDao;
import com.qianfeng.utils.SessionUtils;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 二涵upup
 * 2019/11/5 15:48
 */
public class EntryBookDaoImpl extends SessionUtils implements EntryBookDao
{
    public List<EntryBook> findall()
    {
        SqlSession session = null;
        List<EntryBook> list = null;
        try
        {
            Reader resourceAsReader = Resources.getResourceAsReader("mybatis-config.xml");
            SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
            SqlSessionFactory sessionFactory = builder.build(resourceAsReader);
            session = sessionFactory.openSession();


            list = session.selectList("com.qianfeng.dao.EntryBookDao.findall");
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            session.close();
        }
        return list;
    }

    public int insert(EntryBook entryBook)
    {
        int i = 0;
        SqlSession session = null;
        try
        {
            Reader resourceAsReader = Resources.getResourceAsReader("mybatis-config.xml");
            SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
            SqlSessionFactory sessionFactory = builder.build(resourceAsReader);
            session = sessionFactory.openSession();


            i = session.insert("com.qianfeng.dao.EntryBookDao.insert",entryBook);
            session.commit();
        } catch (IOException e)
        {
            e.printStackTrace();
            session.rollback();
        }
        finally
        {
            session.close();
        }
        return i;
    }

    public int update(EntryBook entryBook)
    {
        int i = 0;
        SqlSession session = null;
        try
        {
            Reader resourceAsReader = Resources.getResourceAsReader("mybatis-config.xml");
            SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
            SqlSessionFactory sessionFactory = builder.build(resourceAsReader);
            session = sessionFactory.openSession();


            i = session.update("com.qianfeng.dao.EntryBookDao.update",entryBook);
            session.commit();
        } catch (IOException e)
        {
            e.printStackTrace();
            session.rollback();
        }
        finally
        {
            session.close();
        }
        return i;
    }

    public int delete(int id)
    {
        int i = 0;
        SqlSession session = null;
        try
        {
            Reader resourceAsReader = Resources.getResourceAsReader("mybatis-config.xml");
            SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
            SqlSessionFactory sessionFactory = builder.build(resourceAsReader);
            session = sessionFactory.openSession();


            i = session.delete("com.qianfeng.dao.EntryBookDao.delete",id);
            session.commit();
        } catch (IOException e)
        {
            e.printStackTrace();
            session.rollback();
        }
        finally
        {
            session.close();
        }
        return i;
    }

    public List<EntryBook> findById(int id,int categoryid)
    {
        SqlSession session = null;
        List<EntryBook> list = null;
        try
        {
            Reader resourceAsReader = Resources.getResourceAsReader("mybatis-config.xml");
            SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
            SqlSessionFactory sessionFactory = builder.build(resourceAsReader);
            session = sessionFactory.openSession();

            Map map = new HashMap();
            map.put("id",id);
            map.put("title",categoryid);
            list = session.selectList("com.qianfeng.dao.EntryBookDao.findById",map);

        } catch (IOException e)
        {
            e.printStackTrace();

        }
        finally
        {
            session.close();
        }
        return list;
    }

    public Map findJuHe()
    {
        Map map = new HashMap();
        SqlSession session =null;
        try
        {
            session= new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config.xml")).openSession();

            map = (Map) session.selectOne("com.qianfeng.dao.EntryBookDao.findJuHe");
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        finally {

        }
        return map;
    }


    public List<EntryBook> findbyids(int[] ids)
    {
        SqlSession sqlSession = getsession();
        List<EntryBook> list = sqlSession.selectList("findbyids", ids);
        sqlSession.close();
        return list;
    }

    public List<EntryBook> findByLike(String title, String summary)
    {
        SqlSession sqlSession = getsession();
        Map map = new HashMap();
        map.put("title",title);
        map.put("summary",summary);
        List<EntryBook> list = sqlSession.selectList("com.qianfeng.dao.EntryBookDao.findByLike", map);
        closesession();
        return list;
    }

    public List<EntryBook> findBetween(String begin, String end)
    {
        SqlSession sqlSession = getsession();
        Map map = new HashMap();
        map.put("begin",begin);
        map.put("end",end);
        List<EntryBook> list = sqlSession.selectList("com.qianfeng.dao.EntryBookDao.findBetween", map);
        closesession();
        return list;
    }
}
